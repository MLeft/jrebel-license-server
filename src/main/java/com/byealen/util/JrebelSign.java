package com.byealen.util;

import org.apache.commons.lang3.StringUtils;

public class JrebelSign {
    private String signature;
    private String serverRandomness = "H2ulzLlh7E0=";//服务端随机数,如果要自己生成，务必将其写到json的serverRandomness中

    public void toLeaseCreateJson(String clientRandomness, String guid, boolean offline, Long validFrom, Long validUntil) {
        this.serverRandomness = ByteUtil.a(ByteUtil.a(8));
        String s2 = "";
        if (offline) {
            s2 = StringUtils.join(new String[]{clientRandomness, serverRandomness, guid, String.valueOf(offline), String.valueOf(validFrom), String.valueOf(validUntil)}, ';');
        } else {
            s2 = StringUtils.join(new String[]{clientRandomness, serverRandomness, guid, String.valueOf(offline)}, ';');
        }
        final byte[] a2 = LicenseServer2ToJRebelPrivateKey.a(s2.getBytes());
        this.signature = ByteUtil.a(a2);
    }

    public String getSignature() {
        return signature;
    }

    public String getServerRandomness() {
        return serverRandomness;
    }
}
