package com.byealen.controller;

import com.byealen.util.Rsasign;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class PageController {

    @RequestMapping(value = "/rpc/ping.action")
    public void pingHandler(HttpServletResponse response, @RequestParam("salt") String salt) throws IOException {
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        if (salt == null) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        } else {
            String xmlContent = "<PingResponse><message></message><responseCode>OK</responseCode><salt>" + salt + "</salt></PingResponse>";
            String xmlSignature = Rsasign.Sign(xmlContent);
            String body = "<!-- " + xmlSignature + " -->" + xmlContent;
            response.getWriter().print(body);
        }
    }

    @RequestMapping(value = "/rpc/obtainTicket.action")
    public void obtainTicketHandler(HttpServletResponse response,
                                    @RequestParam("salt") String salt,
                                    @RequestParam("userName") String username) throws IOException {
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        //SimpleDateFormat fm = new SimpleDateFormat("EEE,d MMM yyyy hh:mm:ss Z", Locale.ENGLISH);
        //String date = fm.format(new Date()) + " GMT";
        //response.setHeader("Date", date);
        //response.setHeader("Server", "fasthttp");
        String prolongationPeriod = "607875500";
        if (salt == null || username == null) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        } else {
            String xmlContent = "<ObtainTicketResponse><message></message><prolongationPeriod>" + prolongationPeriod + "</prolongationPeriod><responseCode>OK</responseCode><salt>" + salt + "</salt><ticketId>1</ticketId><ticketProperties>licensee=" + username + "\tlicenseType=0\t</ticketProperties></ObtainTicketResponse>";
            String xmlSignature = Rsasign.Sign(xmlContent);
            String body = "<!-- " + xmlSignature + " -->" + xmlContent;
            response.getWriter().print(body);
        }
    }

    @RequestMapping(value = "/rpc/releaseTicket.action")
    public void releaseTicketHandler(HttpServletResponse response, @RequestParam("salt") String salt) throws IOException {
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        if (salt == null) {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        } else {
            String xmlContent = "<ReleaseTicketResponse><message></message><responseCode>OK</responseCode><salt>" + salt + "</salt></ReleaseTicketResponse>";
            String xmlSignature = Rsasign.Sign(xmlContent);
            String body = "<!-- " + xmlSignature + " -->" + xmlContent;
            response.getWriter().print(body);
        }
    }
}
