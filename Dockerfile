FROM java:8-jre-alpine
WORKDIR /home
COPY target/jrebel-license-server.jar server.jar
EXPOSE 80
ENTRYPOINT ["java","-jar","server.jar"]